import axios from 'axios';

const getJsonList = () => {
    return axios.get(
        `https://jsonplaceholder.typicode.com/posts`
    );
};

const api = {
    searchCourier: getJsonList
};

export default api;
