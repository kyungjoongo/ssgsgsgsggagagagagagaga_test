import {call, put, takeEvery} from 'redux-saga/effects';
import api from '../api/index';
import {getList, setResults} from "../slices/counterSlice";

function* getListJson() {
    try {
        const {data} = yield call(api.searchCourier);
        console.info("temp====>", data);
        //todo:놓는다
        yield put(setResults(data));
    } catch (error) {
        yield put(setResults([]));
    }
}

function* rootSaga() {
    //todo: dispatch에 의해 action.type이 "getList'인 객체가 올 때 getListJson을 실행시킨다
    yield takeEvery(getList, getListJson);
}

export default rootSaga;
