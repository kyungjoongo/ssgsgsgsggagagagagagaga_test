import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Provider} from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas/counterSaga';
import {configureStore} from '@reduxjs/toolkit'
import counterReducer from "./slices/counterSlice";

const sagaMiddleware = createSagaMiddleware()

const store = configureStore({
    reducer: {
        counter: counterReducer,
    },
    devTools: true,
    middleware: [sagaMiddleware]
})


sagaMiddleware.run(rootSaga)

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root')
);

