import {createSlice} from '@reduxjs/toolkit'


export const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        value: 0,
        results: [],
    },
    reducers: {
        increment: (state) => {
            state.value += 1
        },
        decrement: (state) => {
            state.value -= 1
        },
        incrementByAmount: (state, action) => {
            state.value += action.payload
        },
        setResults: (state, action) => {
            state.results = action.payload
        },
        getList: (state, action) => {
            state.results = action.payload
        }
    },
})

// Action creators are generated for each case reducer function
export const {increment, decrement, incrementByAmount, setResults, getList} = counterSlice.actions

const counterReducer = counterSlice.reducer

export default counterReducer
