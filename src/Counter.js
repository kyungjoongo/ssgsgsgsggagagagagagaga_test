import React from 'react'
import {useSelector, useDispatch} from 'react-redux'

//todo: 액션을 가지고 온다..
import {decrement, increment, incrementByAmount, getList} from './slices/counterSlice'

export function Counter() {
    const results = useSelector((state) => {
        if (state.counter.results !== undefined) {
            return state.counter.results
        } else {
            return []
        }
    })
    const dispatch = useDispatch()

    return (
        <div>
            <div>
                <button
                    aria-label="Increment value"
                    onClick={() => dispatch(getList())}
                >
                    getList
                </button>
                <br/>
                <br/>
                <br/>
                {results.map(item => {
                    return (
                        <div>
                            {item.title}
                        </div>
                    )
                })}

                <br/>
            </div>
        </div>
    )
}
